# R package to analyse the data for Ecoelec project (from home assistant)


To install the package :

Some example data is provided, see the template.
```
library(remotes)
remotes::install_git("https://forgemia.inra.fr/ecoelec/ecoelecAnalysis", build_vignettes=TRUE)
```


# Main workflow

The main workflow of analysis is presented below.

![](man/figures/workflow.png)

There is two sources of data:

- the data extracted from home assistant and
- a csv data where the dates and hours of change of devices has to be recorded.


An example of analysis is done in the vignette: `vignette("Template")`.



