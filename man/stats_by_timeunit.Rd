% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/functions_analysis.R
\name{stats_by_timeunit}
\alias{stats_by_timeunit}
\title{Create a data.frame ready for analysis by time unit}
\usage{
stats_by_timeunit(
  stats_data,
  unit = "day",
  groupvar = "measured_device",
  filter_incompletes = TRUE,
  var_measure = "summation_delivered"
)
}
\arguments{
\item{stats_data}{The data containing the measures with de metadata}

\item{unit}{a string, Period object . Time unit to be rounded to.
Valid base units are second, minute, hour, day, week, month, bimonth,
quarter, season, halfyear and year.}

\item{groupvar}{a character, name of the grouping variable}

\item{filter_incompletes}{logical, default to TRUE. Should incomplete units of time should be filtered ?}

\item{var_measure}{character. Variable to measure. Default to "summation_delivered"}
}
\value{
a data.table
}
\description{
Create a data.frame ready for analysis by time unit
}
